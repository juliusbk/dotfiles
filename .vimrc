" Turn on line numbering. Turn it off with "set nonu" 
set nu 

" Set syntax on
syntax on

" Indent automatically depending on filetype
filetype indent on
set autoindent

" Case insensitive search
set ic

" Higlhight search
set hls

" Wrap text instead of being on one line
set lbr

" Change colorscheme from default to delek
colorscheme delek

" Share clipboard
set clipboard=unnamedplus

" (sw)shiftwidth: how many columns text is indented with reindent operations
" (sts)softtabstop: how many columns vim uses when you hit tab
" (ts)tabstop: how many columns a tab counts for
set ts=4 sw=4 sts=4

" Soft tabs
set expandtab
